import json

from app.app import create_app
from app.utils import get_image

app = create_app()
unknown_avatar = get_image(app.config['AVATAR_URL'].format('49999'))

cards_data = None

with open(f'{app.config["ROOT_DIR"]}/app/cards.json', encoding='utf-8') as file:
    cards_data = json.loads(file.read())

if __name__ == '__main__':
    if app.config['FLASK_ENV'] == 'production':
        app.run()
    else:
        app.run(port=5000, debug=True)
