import requests


def get_image(url):
    result = requests.get(url)
    if result.ok:
        return result.content
    else:
        raise FileNotFoundError
