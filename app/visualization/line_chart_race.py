import matplotlib
import matplotlib.pyplot as plt
from bar_chart_race import prepare_wide_data
from bar_chart_race._line_chart_race import _LineChartRace

matplotlib.rcParams['animation.ffmpeg_args'] = ['-preset', 'ultrafast']


class LineChartRace(_LineChartRace):
    def create_figure(self):
        fig = plt.Figure(**self.fig_kwargs)
        ax = fig.add_subplot()
        fig.subplots_adjust(left=0.1, bottom=0.1, right=0.95, top=0.9, wspace=None, hspace=None)
        self.prepare_axes(ax)
        return fig

    def prepare_data(self, df):
        sort = 'desc'
        interpolate_period = False
        compute_ranks = sort = True
        orientation = 'h'
        values, ranks = prepare_wide_data(df, orientation, sort, self.n_lines, interpolate_period,
                                          self.steps_per_period, compute_ranks)

        idx = values.iloc[-1].sort_values(ascending=False).index
        top_cols, other_cols = idx[:self.n_lines], idx[self.n_lines:]
        all_values = values.copy()
        values, ranks, others = values[top_cols], ranks[top_cols], values[other_cols]

        if self.others_line_func in (None, True) or len(other_cols) == 0:
            others_agg_line = None
        else:
            others_agg_line = self.prepare_others_agg_line(others)
        return all_values, values, ranks, others, others_agg_line

    def prepare_axes(self, ax):
        ax.grid(True, color='white')
        ax.tick_params(labelsize=self.tick_label_font['size'], length=0, pad=2)
        ax.minorticks_off()
        ax.set_axisbelow(True)
        ax.set_facecolor('.9')
        ax.set_title(**self.title)

        min_val = self.df_values.min().min()
        max_val = self.df_values.max().max()

        if self.others_line_func is True:
            min_val = min(min_val, self.df_others.min().min())
            max_val = max(max_val, self.df_others.max().max())
        elif self.others_agg_line is not None:
            min_val = min(min_val, self.others_agg_line.min())
            max_val = max(max_val, self.others_agg_line.max())

        if self.agg_line is not None:
            min_val = min(min_val, self.agg_line.min())
            max_val = max(max_val, self.agg_line.max())
        min_val = 1 if self.scale == 'log' else min_val

        ax.set_yscale(self.scale)

        for spine in ax.spines.values():
            spine.set_visible(False)

        ax.set_xlim(self.df_values.index[0], self.df_values.index[-1])
        ax.set_ylim(min_val, max_val)
        xmin, xmax = ax.get_xlim()
        delta = (xmax - xmin) * .05
        ax.set_xlim(xmin - delta, xmax + delta)

        ymin, ymax = ax.get_ylim()
        delta = (ymax - ymin) * .05
        if self.scale == 'log':
            delta = 0
            ymax = 2 * ymax
        ax.set_ylim(ymin - delta, ymax + delta)

        if self.is_x_date:
            ax.xaxis_date()
            # ax.xaxis.set_major_formatter(mdates.DateFormatter('%-m/%-d'))

        if self.tick_template:
            ax.yaxis.set_major_formatter(self.tick_template)

    @staticmethod
    def create(df, filename=None, n_lines=None, steps_per_period=10,
               period_length=500, end_period_pause=0, period_summary_func=None,
               line_width_data=None, agg_line_func=None, agg_line_kwargs=None,
               others_line_func=None, others_line_kwargs=None, fade=1, min_fade=.3,
               images=None, colors=None, title=None, line_label_font=None,
               tick_label_font=None, tick_template='{x:,.0f}', shared_fontdict=None,
               scale='linear', fig=None, writer=None, line_kwargs=None,
               fig_kwargs=None):
        return LineChartRace(df, filename, n_lines, steps_per_period, period_length, end_period_pause,
                             period_summary_func, line_width_data, agg_line_func, agg_line_kwargs,
                             others_line_func, others_line_kwargs, fade, min_fade, images, colors,
                             title, line_label_font, tick_label_font, tick_template, shared_fontdict,
                             scale, fig, writer, line_kwargs, fig_kwargs)
