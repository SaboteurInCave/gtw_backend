import matplotlib.pyplot as plt
from bar_chart_race._bar_chart_race import _BarChartRace


class BarChartRace(_BarChartRace):
    def create_figure(self):
        fig = plt.Figure(**self.fig_kwargs)
        ax = fig.add_subplot()
        fig.subplots_adjust(left=0.1, bottom=0.1, right=0.95, top=0.9, wspace=None, hspace=None)
        self.prepare_axes(ax)
        self.fix_axis_limits(ax)
        return fig

    @staticmethod
    def create(df, filename=None, orientation='h', sort='desc', n_bars=None,
               fixed_order=False, fixed_max=False, steps_per_period=10,
               period_length=500, end_period_pause=0, interpolate_period=False,
               period_label=True, period_template=None, period_summary_func=None,
               perpendicular_bar_func=None, colors=None, title=None, bar_size=.95,
               bar_textposition='outside', bar_texttemplate='{x:,.0f}',
               bar_label_font=None, tick_label_font=None, tick_template='{x:,.0f}',
               shared_fontdict=None, scale='linear', fig=None, writer=None,
               bar_kwargs=None, fig_kwargs=None, filter_column_colors=False):
        return BarChartRace(df, filename, orientation, sort, n_bars, fixed_order, fixed_max,
                            steps_per_period, period_length, end_period_pause, interpolate_period,
                            period_label, period_template, period_summary_func, perpendicular_bar_func,
                            colors, title, bar_size, bar_textposition, bar_texttemplate,
                            bar_label_font, tick_label_font, tick_template, shared_fontdict, scale,
                            fig, writer, bar_kwargs, fig_kwargs, filter_column_colors)
