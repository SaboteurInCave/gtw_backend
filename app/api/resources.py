import io

import numpy as np
import requests
from PIL import Image
from flask import send_file, request
from flask_restplus import Resource

from app.api import api_rest
from app.deckAnalysis.deck_analyzer import DeckAnalyzer
from app.reports.tournament.GwentTournamentsManager import GwentTournamentsManager
from app.utils import get_image


# request.json


@api_rest.route('/user_image/<int:avatar>/<int:border>/')
@api_rest.route('/user_image/<int:avatar>/')
@api_rest.route('/user_image/')
class SeasonUsers(Resource):
    def get_new_image(self, image_id: int):
        from run import app

        image_url = app.config['NEW_IMAGE_URL'].format(int(str(image_id)[:2]) + 1, image_id)
        return get_image(image_url)

    def get(self, avatar=-1, border=-1):
        from run import unknown_avatar

        try:
            avatar_image = self.get_new_image(avatar)
        except FileNotFoundError:
            avatar_image = unknown_avatar

        try:
            border_image = self.get_new_image(border)
        except FileNotFoundError:
            # problems with border
            return send_file(io.BytesIO(avatar_image), mimetype='image/png', as_attachment=True,
                             attachment_filename='unknown_border.png')

        avatar_image = Image.open(io.BytesIO(avatar_image)).convert("RGBA")
        border_image = Image.open(io.BytesIO(border_image)).convert("RGBA")

        result = Image.new('RGBA', (300, 300), (255, 0, 0, 0))
        avatar_image.thumbnail((120, 120), Image.ANTIALIAS)

        result.paste(avatar_image, (90, 90), avatar_image)
        result.paste(border_image, (0, 0), border_image)

        image_data = np.asarray(result)
        image_data_bw = image_data.max(axis=2)
        non_empty_columns = np.all(image_data_bw == image_data_bw[0, :],
                                   axis=0)  # np.where(image_data_bw.all(,axis=0))[0]
        non_empty_rows = np.all(image_data_bw == image_data_bw[0, :], axis=1)  # np.where(image_data_bw.all(axis=1))[0]

        columns_indexes = []
        for index, item in enumerate(non_empty_columns):
            if not item:
                columns_indexes.append(index)

        columns_rows = []
        for index, item in enumerate(non_empty_rows):
            if not item:
                columns_rows.append(index)

        cropBox = (min(columns_rows), max(columns_rows), min(columns_indexes), max(columns_indexes))
        image_data_new = image_data[cropBox[0]:cropBox[1] + 1, cropBox[2]:cropBox[3] + 1, :]

        result = Image.fromarray(image_data_new)
        result_bytes = io.BytesIO()
        result.save(result_bytes, format='PNG')

        result_bytes.seek(0)

        return send_file(result_bytes, mimetype='image/png', as_attachment=True,
                         attachment_filename=f'{avatar}_{border}.png')


@api_rest.route('/tournament/<int:tournament_id>')
class TournamentParser(Resource):
    def get(self, tournament_id: int):
        from run import app

        gog_token = app.config["GOG_TOKEN"]

        tournament = requests.get(f'https://tournaments.playgwent.com/api/tournament/{tournament_id}',
                                  headers={'Authorization': f'Basic {gog_token}'})

        games = requests.get(f'https://tournaments.playgwent.com/api/tournament/{tournament_id}/games',
                             headers={'Authorization': f'Basic {gog_token}'})

        if tournament.ok and games.ok:
            tournament_data = tournament.json()
            games_data = games.json()

            parser = GwentTournamentsManager(tournament_data, games_data, gog_token)

            # parse and write data
            parser.generate_tournament_participants()
            parser.generate_tournament_bans()
            parser.generate_factions_statistics()
            parser.generate_winrate_statistics()

            # return data
            return send_file(parser.get_content(),
                             mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                             as_attachment=True,
                             attachment_filename=f'{tournament_data["name"]}.xlsx')
        else:
            if not tournament.ok:
                app.logger.error(tournament.reason)
                return 400

            if not games.ok:
                app.logger.error(games.reason)
                return 400


@api_rest.route('/deck_analyzer')
class DeckAnalyzerRoute(Resource):
    def post(self):
        json_data = request.json
        deck_analyzer = DeckAnalyzer(json_data['link'])
        try:
            deck_analyzer.load_deck()
            return deck_analyzer.analyze_process()
        except Exception as error:
            from run import app
            app.logger.error(error, exc_info=True)
            return "Error", 400

# @api_rest.route('/charts/leaderboards_race')
# class LeaderboardsRace(Resource):
#     def post(self):
#         json_data = request.json
#
#         pd_frame = pd.DataFrame(json_data['data'][1:], columns=json_data['data'][0])
#         pd_frame = pd_frame.drop_duplicates(subset=['timestamp', 'name'], keep='first')
#
#         pd_frame["timestamp"] = pd.to_datetime(pd_frame["timestamp"], unit='ms')
#
#         df_values, df_ranks = bcr.prepare_long_data(pd_frame, index='timestamp', columns='name',
#                                                     values='value', steps_per_period=1)
#
#         print(df_values)
#         print(df_ranks)
#
#         dpi = 200
#         w = 1920
#         h = 1080
#
#         result_modified = BarChartRace.create(df_values[:10], n_bars=10, filename='test.mp4',
#                                               title={'label': "Сезон Дриад 2020", 'size': 14},
#                                               fixed_max=True,
#                                               period_template='%d-%m-%Y %H:%M',
#                                               fig_kwargs={
#                                                   'figsize': (w / dpi, h / dpi),
#                                                   'dpi': 200,
#                                               },
#                                               bar_label_font={'size': 12},
#                                               steps_per_period=25,
#                                               period_length=750,
#                                               filter_column_colors=True,
#                                               period_label=dict(x=0.90, y=-0.05, ha='center', va='top'))
#
#         result = result_modified.make_animation()
#         return result
#
#
# @api_rest.route('/charts/meta_race')
# class MetaRace(Resource):
#     def post(self):
#         from run import app
#
#         json_data = request.json
#         start, finish = json_data['start'], json_data['finish']
#         mode = 'Pro Rank'
#
#         print(app.config['DATABASE_ADDRESS'], app.config['DATABASE_NAME'])
#
#         client = MongoClient(host=app.config['DATABASE_ADDRESS'])
#         db = client[app.config['DATABASE_NAME']]
#
#         timestamps = db['stats'].distinct("timestamp", {
#             'profile.mode': mode,
#             'timestamp': {
#                 '$gte': start,
#                 '$lte': finish
#             }
#         })
#
#         results = list(db['stats'].find({
#             'profile.mode': mode,
#             'timestamp': {
#                 '$gte': start,
#                 '$lte': finish
#             }
#         }))
#
#         columns = ['NR', 'NG', 'ST', 'MO', 'SK', 'SY']
#         data = []
#
#         for timestamp in timestamps:
#             users = list(filter(lambda x: x['timestamp'] == timestamp, results))
#             used_users = set()
#
#             winrate_data = {faction: {'wins': 0, 'total': 0, 'winrate': 0} for faction in columns}
#
#             for user in users:
#                 username = user['profile']['name']
#                 if username not in used_users:
#                     used_users.add(username)
#
#                     for faction in columns:
#                         faction_label = faction
#                         if faction == 'ST':
#                             faction_label = 'SC'
#
#                         winrate_data[faction]['wins'] += user['wins']['season'][faction_label]
#                         winrate_data[faction]['total'] += user['season']['factions'][faction_label]['games']
#
#                         winrate = winrate_data[faction]['wins'] / winrate_data[faction]['total'] if \
#                             winrate_data[faction]['total'] > 0 else 0
#                         winrate_data[faction]['winrate'] = winrate * 100
#
#             result = [timestamp]
#             for faction in columns:
#                 result.append(winrate_data[faction]['winrate'])
#
#             data.append(result)
#
#         pd_frame = pd.DataFrame(data, columns=['timestamp', *columns])
#         pd_frame["timestamp"] = pd.to_datetime(pd_frame["timestamp"], unit='ms')  # , unit='ms')
#         pd_frame = pd_frame.set_index('timestamp')
#         print(pd_frame)
#
#         result = LineChartRace.create(pd_frame, filename='meta_race.mp4', fade=0.5, steps_per_period=25,
#                                       period_length=750, colors=[
#                 '#2196F3', '#212121', '#4CAF50', '#F44336', '#9C27B0', '#FF9800'
#             ])
#         result.make_animation()
#
#         return 200
