import json
from pprint import pprint

from xlsxwriter import Workbook

from app.enums import GwentDataFactions

language = 'ru-RU'

if __name__ == '__main__':
    with open(f'../cards.json', encoding='utf-8') as file:
        cards_data = json.loads(file.read())

    factions_data = {x.value: [] for x in GwentDataFactions}

    for card_id, card in cards_data.items():
        factions_data[card['faction']].append(card)

    workbook = Workbook("Card Analysis.xlsx")

    workbook.add_worksheet("Памятка")

    bold_center_format = workbook.add_format({
        'bold': True,
        'align': 'center',
        'valign': 'vcenter',
    })

    default_format = workbook.add_format({
        'align': 'center',
        'valign': 'vcenter',
        'text_wrap': True
    })

    for faction in GwentDataFactions:
        faction_cards = list(
            filter(lambda x: x['released'] is True, sorted(factions_data[faction.value], key=lambda x: x['ingameId'])))
        sheet = workbook.add_worksheet(faction.value)

        sheet.write_string(0, 0, "Название", bold_center_format)
        sheet.write_string(0, 1, "ID", bold_center_format)
        sheet.write_string(0, 2, "Тип карты", bold_center_format)
        sheet.write_string(0, 3, "Цвет карты", bold_center_format)
        sheet.write_string(0, 4, "Провизия", bold_center_format)
        sheet.write_string(0, 5, "Сила", bold_center_format)
        sheet.write_string(0, 6, "Броня", bold_center_format)
        sheet.write_string(0, 7, "Описание", bold_center_format)
        sheet.write_string(0, 8, "Категории", bold_center_format),
        sheet.write_string(0, 9, "Ключевые слова", bold_center_format),
        sheet.write_string(0, 10, "Лояльность", bold_center_format),
        sheet.write_string(0, 11, "Позиции", bold_center_format),
        sheet.write_string(0, 12, "Статусы", bold_center_format),

        row_index = 1
        for card in faction_cards:
            sheet.write_row(row_index, 0, [
                card['name'][language],
                int(card['ingameId']),
                card['cardType'],
                card['type'],
                card['provision'],
                card['strength'],
                card.get('armor', 0),
                card['infoRaw'][language],
                ", ".join(card['categories']),
                ", ".join(card['keywords']),
                ", ".join(card['loyalties']),
                ", ".join(card['positions']),
                card["statusListRaw"]
            ], default_format)

            row_index += 1

    workbook.close()
