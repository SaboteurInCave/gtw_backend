from enum import Enum


class Factions(Enum):
    NR = 'NR'
    NG = 'NG'
    SC = 'SC'  # Scoatael
    MO = 'MO'
    SK = 'SK'
    NT = 'NT'
    SY = 'SY'


class GwentDataFactions(Enum):
    NR = 'Northern Realms'
    NG = 'Nilfgaard'
    SC = 'Scoiatael'
    MO = 'Monster'
    SK = 'Skellige'
    NT = 'Neutral'
    SY = 'Syndicate'
