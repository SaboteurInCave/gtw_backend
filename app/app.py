from datetime import datetime
from json import JSONEncoder

from dotenv import load_dotenv
from flask import Flask

from app.api import api_bp


class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, datetime):
                return obj.isoformat()

            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


def create_app():
    app = Flask(__name__)
    load_dotenv()

    app.json_encoder = CustomJSONEncoder
    app.register_blueprint(api_bp)

    app.config.from_object('app.config.Config')

    return app
