import html
import json
import re
from typing import List, Dict

import requests


class DeckAnalyzer:
    def __init__(self, link: str, locale='ru-RU'):
        self.link = link
        self.locale = locale
        self.used_cards = []  # used cards from db
        self.raw_cards = []  # raw deck from the link
        self.order = []  # order of cards
        self.deck = {}
        self.cards_count = 0
        self.crafting_cost = 0
        self.total_provision = 0

        # 'cards_count': self.deck['cardsCount'],
        # 'crafting_cost': self.deck['craftingCost'],
        # 'total_provision': self.deck['provisionsCost'],

        from run import cards_data
        self.cards_data = cards_data

        # inner constants
        self.ROW_CAP = 9

    def load_deck(self):
        pattern = re.compile("data-state=\'(...+)\'", re.MULTILINE)

        response = requests.get(self.link, headers={
            'Content-Type': 'text/plain',
        })

        if response.ok:
            text = html.unescape(response.text)
            result = pattern.search(text)
            data = json.loads(result.group(1))

            self.deck = data['guide']['deck'] if 'guide' in data else data['deck']
            self.raw_cards = self.deck['cards']
            self.cards_count = self.deck['cardsCount']
            self.crafting_cost = self.deck['craftingCost']
            self.total_provision = self.deck['provisionsCost']

            self.order = list(map(lambda x: x['id'], self._order_cards(self.raw_cards)))
            self.used_cards = self._gather_cards()
        else:
            raise ConnectionError

    def _order_cards(self, raw_cards: List[Dict]):
        return sorted(raw_cards, key=lambda x: (-x['provisionsCost'], x['type'], -x['power'], x['id']))

    def _gather_cards(self):
        cards = self.cards_data.items()
        used_cards = []

        for card_id, card in cards:
            if int(card_id) in self.order:
                used_cards.append(card)

        print('used_cards', used_cards)
        return used_cards

    def analyze_process(self):
        result = {
            'total_raw_strength': 0,
            'cards_count': self.cards_count,
            'crafting_cost': self.crafting_cost,
            'total_provision': self.total_provision,
            'colors': {},
            'types': {},
            'cards': [],
            'leader': {},
            'stratagem': {}
        }

        # leader
        db_card_leader = next(
            filter(lambda x: int(x['ingameId']) == self.deck['leader']['id'], self.cards_data.values()))
        result['leader'] = {
            'name': db_card_leader['name'][self.locale],
            'provision': db_card_leader['provision']
        }

        # strategeme
        db_card_stratagem = next(
            filter(lambda x: int(x['ingameId']) == self.deck['stratagem']['id'], self.cards_data.values()))

        result['stratagem'] = {
            'name': db_card_stratagem['name'][self.locale],
        }

        # deck cards
        for card_id in self.order:
            raw_card = next(filter(lambda x: x['id'] == card_id, self.raw_cards))
            db_card = next(filter(lambda x: int(x['ingameId']) == card_id, self.used_cards))

            # card analysis
            result['total_raw_strength'] += db_card['strength'] * (raw_card['repeatCount'] + 1)

            card_color = db_card['type']
            if card_color not in result['colors']:
                result['colors'][card_color] = 0

            result['colors'][card_color] += 1 * (raw_card['repeatCount'] + 1)

            card_type = db_card['cardType']
            if card_type not in result['types']:
                result['types'][card_type] = 0

            result['types'][card_type] += 1 * (raw_card['repeatCount'] + 1)

            result['cards'].append({
                'name': db_card['name'][self.locale],
                'type': db_card['cardType'],
                'color': db_card['type'],
                'strength': db_card['strength'],
                'provision': db_card['provision'],
                'armor': db_card.get('armor', 0),
                'count': raw_card['repeatCount'] + 1
            })

        return result
