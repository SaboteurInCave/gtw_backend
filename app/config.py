import os


class Config(object):
    # If not set fall back to production for safety
    FLASK_ENV = os.getenv('FLASK_ENV', 'debug')
    # Set FLASK_SECRET on your production Environment
    SECRET_KEY = os.getenv('FLASK_SECRET', 'Secret')
    GOG_TOKEN = os.getenv('GOG_TOKEN', 'INVALID_GOG_TOKEN')
    DATABASE_ADDRESS = os.getenv('MONGO_HOST', 'localhost')
    DATABASE_NAME = os.getenv('DATABASE', '')

    APP_DIR = os.path.dirname(__file__)
    ROOT_DIR = os.path.dirname(APP_DIR)

    AVATAR_URL = 'https://cdn-l-playgwent.cdprojektred.com/avatars/{}.jpg'
    BORDER_URL = 'https://cdn-l-playgwent.cdprojektred.com/borders/{}.png'

    NEW_IMAGE_URL = 'https://www.playgwent.com/uploads/media/assets_item_preview/0001/{}/' \
                    'thumb_{}_assets_item_preview_big.png'
