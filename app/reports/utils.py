from enum import Enum


class Factions(Enum):
    NR = 'Northern Realms'
    NG = 'Nilfgaard'
    ST = 'Scoiatael'
    MO = 'Monster'
    SK = 'Skellige'
    SY = 'Syndicate'


faction_labels = {
    Factions.NR.value: '[NR]',
    Factions.NG.value: '[NG]',
    Factions.ST.value: '[ST]',
    Factions.MO.value: '[MO]',
    Factions.SK.value: '[SK]',
    Factions.SY.value: '[SY]'
}

faction_colors = {
    Factions.NR: '#c9daf8',
    Factions.NG: '#cccccc',
    Factions.ST: '#d9ead3',
    Factions.MO: '#f4cccc',
    Factions.SK: '#d9d2e9',
    Factions.SY: '#fce5cd'
}

coin_color = [
    '#a4c2f4',
    '#ea9999'
]

class Sheets(Enum):
    PARTICIPANTS = 'participants'
    BANS = 'bans'
    FACTION_STATISTICS = 'faction_statistics'
    WINRATE_STATISTICS = 'winrate_statistics'


api_brackets = [
    'winner', 'loser', 'third', 'final'
]


# functions

def get_card_localization(cards, card_id, locale='en-US', faction_label=False):
    card_id = str(card_id)

    if card_id not in cards:
        return 'Unknown'

    card = cards[card_id]
    label = card['name'][locale]

    if faction_label:
        label = f'{faction_labels[card["faction"]]} ' + label

    return label
