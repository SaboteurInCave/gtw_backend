import copy
import io
from typing import Dict, List

import requests
from flask import current_app
from xlsxwriter import Workbook
from xlsxwriter.utility import xl_range_abs, xl_rowcol_to_cell, xl_range
from xlsxwriter.worksheet import Worksheet

from app.reports.utils import Sheets, get_card_localization, Factions, api_brackets, faction_colors, coin_color


class GwentTournamentsManager:
    def __init__(self, tournament_data: Dict, games_data: Dict, gog_token: str):
        self.file = io.BytesIO()
        self.workbook = Workbook(self.file)

        self.tournament_data = tournament_data
        self.games_data = games_data
        self.gog_token = gog_token

        from run import cards_data
        self.cards_data = cards_data

        self.tournament_id = self.tournament_data['id']
        self.picks_count = self.tournament_data['picksCount']
        self.participants = self.tournament_data['participants']

        self.participants_id_list = list(map(lambda x: x['id'], self.participants))
        self.games = self._download_games()
        self.factions_order = self._get_factions_order()
        current_app.logger.debug(self.factions_order)

        self.sheets = {
            Sheets.PARTICIPANTS: 'Participants',
            Sheets.BANS: 'Bans',
            Sheets.FACTION_STATISTICS: 'Factions statistics',
            Sheets.WINRATE_STATISTICS: 'Winrate statistics'
        }

    def get_content(self) -> io.BytesIO:
        """
        Final function - return all generated data
        :return: bytes representation
        """

        self.workbook.close()
        self.file.seek(0)

        return self.file

    def _download_games(self) -> Dict:
        result = {
            api_bracket: [] for api_bracket in api_brackets
        }

        url = 'https://tournaments.playgwent.com/api/tournament/{}/match/{}'

        for game_type in result.keys():
            if game_type in self.games_data:

                # finals and third matches are not list, for code compatibility we will change it
                if not isinstance(self.games_data[game_type], list):
                    deep_copy = copy.deepcopy(self.games_data[game_type])
                    temp_array = [[deep_copy]]
                    self.games_data[game_type] = temp_array

                for round_index, game_bracket in enumerate(self.games_data[game_type]):
                    entry = {
                        'round': round_index + 1,
                        'games': []
                    }

                    for game in game_bracket:
                        current_app.logger.info(f'{game_type} - {game}')
                        if game and 'id' in game:
                            entry['games'].append(
                                requests.get(url.format(self.tournament_id, game["id"]),
                                             headers={'Authorization': f'Basic {self.gog_token}'}).json()
                            )

                    result[game_type].append(entry)

        return result

    def _get_factions_order(self):
        result = {
            'order': [],
            'factions': {x: [] for x in Factions},
        }

        faction_abilities_count = {x: {} for x in Factions}

        # calculate all leader usage
        for faction in faction_abilities_count.keys():
            faction_leaders = {
                card_id: entry for card_id, entry in self.cards_data.items() if
                entry['cardType'] == "Leader" and entry['faction'] == faction.value
            }

            for card_id, entry in faction_leaders.items():
                faction_abilities_count[faction][int(card_id)] = 0

            for participant in self.participants:
                decks = participant['decks']
                for deck in decks:
                    leader_id = deck['leaderId']
                    if leader_id in faction_abilities_count[faction]:
                        faction_abilities_count[faction][leader_id] += 1

        temp_faction_list = [
            [faction, 0, index] for index, faction in enumerate(Factions)
        ]

        for i in range(len(temp_faction_list)):
            faction = temp_faction_list[i][0]
            for value in faction_abilities_count[faction].values():
                temp_faction_list[i][1] += value

        temp_faction_list = sorted(temp_faction_list, key=lambda k: (k[1], k[2]), reverse=True)
        result['order'] = list(map(lambda k: k[0], temp_faction_list))

        for faction in result['order']:
            faction_result = list(faction_abilities_count[faction].items())
            faction_result = sorted(faction_result, key=lambda x: (x[1], x[0]), reverse=True)
            result['factions'][faction] = list(map(lambda x: x[0], faction_result))

        return result

    def _get_games_count(self):
        result = 0

        for api_bracket in api_brackets:
            result += len(self.games[api_bracket])

        return result

    def _create_worksheet(self, name: Sheets):
        worksheet_label = self.sheets[name]
        worksheet = self.workbook.get_worksheet_by_name(worksheet_label)

        if not worksheet:
            worksheet = self.workbook.add_worksheet(worksheet_label)

        return worksheet

    def generate_tournament_participants(self):
        """
        Generate tournament participants list with deck links
        """

        worksheet = self._create_worksheet(Sheets.PARTICIPANTS)

        # set column width
        worksheet.set_column(0, 0, 25)
        worksheet.set_column(1, self.picks_count, 25)
        worksheet.set_column(self.picks_count + 1, 1 + 2 * self.picks_count, 15)

        row_index = 0

        username_label = 'Username'
        ability_label = 'Leader ability {}'
        deck_label = 'Deck {} (link)'

        worksheet.write(row_index, 0, username_label)
        for i in range(self.picks_count):
            worksheet.write(row_index, 1 + i, ability_label.format(i + 1))
            worksheet.write(row_index, 1 + i + self.picks_count, deck_label.format(i + 1))

        # make first row "pinned"
        worksheet.freeze_panes(1, 1)

        row_index += 1

        # participants parsing
        for participant in self.participants:
            worksheet.write(row_index, 0, participant['name'])

            decks = participant['decks']
            for i in range(self.picks_count):
                if i < len(decks):
                    worksheet.write(row_index, 1 + i,
                                    get_card_localization(
                                        cards=self.cards_data,
                                        card_id=decks[i]['leaderId'],
                                        faction_label=True)
                                    )

                    if 'link' in decks[i]:
                        worksheet.write_url(row_index, 1 + i + self.picks_count, decks[i]['link'], string='Link')

            row_index += 1

    def generate_tournament_bans(self):
        """
        Generate tournament participants list with bans
        """

        worksheet = self._create_worksheet(Sheets.BANS)

        # set column width
        worksheet.set_column(0, 0, 25)
        worksheet.set_column(1, self.picks_count, 25)
        worksheet.set_column(self.picks_count + 1, self.picks_count + self._get_games_count(), 25)

        row_index = 0

        username_label = 'Username'
        ability_label = 'Leader ability {}'
        ban_label = 'Ban ({}, # {})'

        worksheet.write(row_index, 0, username_label)
        for i in range(self.picks_count):
            worksheet.write(row_index, 1 + i, ability_label.format(i + 1))

        index = 0
        base_ban_index = 1 + self.picks_count

        for api_bracket in api_brackets:
            for i in range(len(self.games[api_bracket])):
                worksheet.write(row_index, base_ban_index + index, ban_label.format(api_bracket, i + 1))
                index += 1

        # make first row "pinned"
        worksheet.freeze_panes(1, 1)

        row_index += 1
        ban_list = self._generate_ban_list()
        current_app.logger.info(ban_list)

        for index, player_id in enumerate(self.participants_id_list):
            worksheet.write(row_index, 0, self.participants[index]['name'])

            decks = self.participants[index]['decks']
            for i in range(self.picks_count):
                if i < len(decks):
                    worksheet.write(row_index, 1 + i,
                                    get_card_localization(
                                        cards=self.cards_data,
                                        card_id=decks[i]['leaderId'],
                                        faction_label=True)
                                    )

            # write participant bans

            index = 0
            for api_bracket in api_brackets:
                for i in range(len(self.games[api_bracket])):
                    worksheet.write_string(
                        row_index,
                        base_ban_index + index,
                        ban_list[player_id][api_bracket][i] if ban_list[player_id][api_bracket][i] else ''
                    )

                    index += 1

            row_index += 1

    def _generate_ban_list(self):
        bans_list = {
            user_id: {api_bracket: [] for api_bracket in api_brackets} for user_id in self.participants_id_list
        }

        for user_id, bans in bans_list.items():
            for api_bracket in api_brackets:
                bans[api_bracket] = [None] * len(self.games[api_bracket])

        def _get_banned_faction(first: Dict, second: Dict) -> List:
            result = []

            for banned_leader in first['ban']:
                deck = next((x for x in second['decks'] if x['id'] == int(banned_leader)), None)
                if deck:
                    result.append(
                        get_card_localization(
                            cards=self.cards_data,
                            card_id=deck['leaderId'],
                            faction_label=True)
                    )

            return result

        for api_bracket in api_brackets:
            for index, game_bracket in enumerate(self.games[api_bracket]):
                games = game_bracket['games']
                for entry in games:
                    first_player = entry['players'][0]
                    second_player = entry['players'][1]

                    if first_player['id']:
                        first_ban = _get_banned_faction(first_player, second_player)
                        current_app.logger.debug(f"{first_player['id']} - {first_ban}")
                        if len(first_ban) == 1:
                            bans_list[first_player['id']][api_bracket][index] = first_ban[0]
                        else:
                            bans_list[first_player['id']][api_bracket][index] = first_ban

                    if second_player['id']:
                        second_ban = _get_banned_faction(second_player, first_player)
                        current_app.logger.debug(f"{second_player['id']} - {second_ban}")
                        if len(second_ban) == 1:
                            bans_list[second_player['id']][api_bracket][index] = second_ban[0]
                        else:
                            bans_list[second_player['id']][api_bracket][index] = second_ban

        return bans_list

    def generate_factions_statistics(self):
        """
        Generate faction usage list
        :return:
        """

        worksheet = self._create_worksheet(Sheets.FACTION_STATISTICS)
        participants_count = len(self.participants)

        row_index = 0

        participants_count_label = 'Количество игроков'

        worksheet.set_column(0, 5, 25)
        worksheet.set_column(6, 6, 27)

        participants_count_cell = xl_rowcol_to_cell(row_index, 1)
        worksheet.write(row_index, 0, participants_count_label)
        worksheet.write(row_index, 1, participants_count)

        row_index += 2

        leader_ability_label = 'Способность лидера'
        worksheet.write(row_index, 0, leader_ability_label)

        row_index += 1

        # calculate total row count for total amount of decks
        total_leaders = len([
            entry for card_id, entry in self.cards_data.items() if entry['cardType'] == "Leader"
        ])

        total_decks_cell = xl_rowcol_to_cell(3 + len(Factions) * 3 + total_leaders, 2, True, True)
        total_bans_cell = xl_rowcol_to_cell(3 + len(Factions) * 3 + total_leaders, 4, True, True)

        total_picks = []
        total_bans = []

        for faction in self.factions_order['order']:
            row_index, total_faction_picks_cell, total_faction_bans_cell \
                = self._generate_faction_block(worksheet, faction,
                                               self.factions_order['factions'][faction],
                                               faction_colors[faction],
                                               participants_count,
                                               total_decks_cell,
                                               total_bans_cell,
                                               participants_count_cell, row_index)

            total_picks.append(total_faction_picks_cell)
            total_bans.append(total_faction_bans_cell)

        final_label = 'Итого'

        bold_right_format = self.workbook.add_format({
            'bold': True,
            'align': 'right',
            'valign': 'vcenter',
            'pattern': 1,
            'bg_color': '#d0e0e3'
        })

        bold_center_format = self.workbook.add_format({
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'pattern': 1,
            'bg_color': '#d0e0e3'
        })

        worksheet.write_string(row_index, 0, '', bold_right_format)
        worksheet.write_string(row_index, 1, final_label, bold_right_format)
        worksheet.write_formula(row_index, 2, f"=SUM({','.join(total_picks)})", bold_center_format)
        worksheet.write_string(row_index, 3, '', bold_right_format)
        worksheet.write_formula(row_index, 4, f"=SUM({','.join(total_bans)})", bold_center_format)
        worksheet.write_string(row_index, 5, '', bold_right_format)
        worksheet.write_string(row_index, 6, '', bold_right_format)

    def _generate_faction_block(self, worksheet: Worksheet, faction: Factions, faction_list: List, faction_color: str,
                                participants_count: int, total_leaders_cell: str, total_bans_cell: str,
                                participants_count_cell: str,
                                row_index: int):
        total_leaders_label = 'Всего лидеров'
        faction_percentage_label = '% фракции'
        faction_bans_label = 'Баны фракции'
        faction_bans_percentage_label = '% банов фракции'
        faction_popularity_label = 'Популярность фракции'
        translate_label = 'Перевод'
        faction_count_label = 'Количество'
        leader_percentage_label = '% лидеров'
        bans_percentage_label = '% банов'
        leader_popularity_percentage_label = 'Популярность способности'

        header_format = self.workbook.add_format({
            'bold': True,
            'pattern': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': faction_color
        })

        header_italic_format = self.workbook.add_format({
            'bold': True,
            'italic': True,
            'pattern': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': faction_color
        })

        header_percentage_format = self.workbook.add_format({
            'bold': True,
            'pattern': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': faction_color,
            'num_format': 10
        })

        content_format = self.workbook.add_format({
            'pattern': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': faction_color
        })

        content_percentage_format = self.workbook.add_format({
            'pattern': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': faction_color,
            'num_format': 10
        })

        content_left_format = self.workbook.add_format({
            'pattern': 1,
            'align': 'left',
            'valign': 'vcenter',
            'bg_color': faction_color
        })

        # leaders fetching
        # leaders = {
        #     card_id: entry for card_id, entry in self.cards_data.items() if
        #     entry['cardType'] == "Leader" and entry['faction'] == faction.value
        # }

        # print headers
        worksheet.merge_range(row_index, 0, row_index + 1, 0, '', cell_format=header_format)
        worksheet.merge_range(row_index, 1, row_index + 1, 1, faction.value, cell_format=header_format)

        worksheet.write_string(row_index, 2, total_leaders_label, cell_format=header_format)
        worksheet.write_string(row_index, 3, faction_percentage_label, cell_format=header_format)
        worksheet.write_string(row_index, 4, faction_bans_label, cell_format=header_format)
        worksheet.write_string(row_index, 5, faction_bans_percentage_label, cell_format=header_format)
        worksheet.write_string(row_index, 6, faction_popularity_label, cell_format=header_format)

        row_index += 1

        # formula stage with faction leaders picks, bans, etc

        total_picks_count_range = xl_range(row_index + 2, 2, row_index + len(faction_list) + 1, 2)
        total_picks_count_cell = xl_rowcol_to_cell(row_index, 2)

        total_bans_count_range = xl_range(row_index + 2, 4, row_index + len(faction_list) + 1, 4)
        total_bans_count_cell = xl_rowcol_to_cell(row_index, 4)

        worksheet.write_formula(row_index, 2, f"=SUM({total_picks_count_range})", cell_format=header_format)
        worksheet.write_formula(row_index, 3, f"={total_picks_count_cell}/{total_leaders_cell}",
                                cell_format=header_percentage_format)

        worksheet.write_formula(row_index, 4, f"=SUM({total_bans_count_range})", cell_format=header_format)
        worksheet.write_formula(row_index, 5, f"={total_bans_count_cell}/{total_bans_cell}",
                                cell_format=header_percentage_format)

        worksheet.write_formula(row_index, 6, f"={total_picks_count_cell}/{participants_count_cell}",
                                cell_format=header_percentage_format)

        row_index += 1
        worksheet.write_string(row_index, 0, '', cell_format=header_format)
        worksheet.write_string(row_index, 1, translate_label, cell_format=header_italic_format)
        worksheet.write_string(row_index, 2, faction_count_label, cell_format=header_italic_format)
        worksheet.write_string(row_index, 3, leader_percentage_label, cell_format=header_italic_format)
        worksheet.write_string(row_index, 4, faction_count_label, cell_format=header_italic_format)
        worksheet.write_string(row_index, 5, bans_percentage_label, cell_format=header_italic_format)
        worksheet.write_string(row_index, 6, leader_popularity_percentage_label, cell_format=header_italic_format)

        row_index += 1

        # useful ranges
        decks_range = \
            f"{self.sheets[Sheets.PARTICIPANTS]}!{xl_range_abs(1, 1, participants_count + 1, self.picks_count + 1)}"

        bans_range = \
            f"""{self.sheets[Sheets.BANS]}!{xl_range_abs(1, 1 + self.picks_count, 1 + participants_count,
                                                         self.picks_count + self._get_games_count())}"""

        for card_id in faction_list:
            original_label = get_card_localization(
                cards=self.cards_data,
                card_id=str(card_id),
                faction_label=True)

            localized_label = get_card_localization(
                cards=self.cards_data,
                card_id=str(card_id),
                locale='ru-RU')

            worksheet.write_string(row_index, 0, original_label, cell_format=content_left_format)
            worksheet.write_string(row_index, 1, localized_label, cell_format=content_left_format)

            # total amount of leader usage
            leader_cell = xl_rowcol_to_cell(row_index, 0)
            worksheet.write_formula(row_index, 2, f"=COUNTIF({decks_range}, {leader_cell})",
                                    cell_format=content_format)

            leader_count_cell = xl_rowcol_to_cell(row_index, 2)
            worksheet.write_formula(row_index, 3, f"{leader_count_cell}/{total_leaders_cell}",
                                    cell_format=content_percentage_format)

            worksheet.write_formula(row_index, 4, f"=COUNTIF({bans_range}, {leader_cell})", cell_format=content_format)

            bans_count_cell = xl_rowcol_to_cell(row_index, 4)
            worksheet.write_formula(row_index, 5, f"{bans_count_cell}/{total_bans_cell}",
                                    cell_format=content_percentage_format)

            worksheet.write_formula(row_index, 6, f"{leader_count_cell}/{participants_count_cell}",
                                    cell_format=content_percentage_format)

            row_index += 1

        return row_index, total_picks_count_cell, total_bans_count_cell

    def generate_winrate_statistics(self):

        def _get_game_bracket_leader(game_entry, number):
            if game_entry[f'leader{number}'] != 0:
                return game_entry[f'leader{number}']
            elif game_entry[f'deck{number}'] and 'leaderId' in game_entry[f'deck{number}']:
                return game_entry[f'deck{number}']["leaderId"]
            else:
                return None

        worksheet = self._create_worksheet(Sheets.WINRATE_STATISTICS)
        leader_col_start = 2

        default_format = self.workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': True
        })

        row_index = 0

        worksheet.freeze_panes(4, 1)

        # get set of chosen leaders
        faction_leaders = set()

        for participant in self.participants:
            decks = participant['decks']
            for i in range(self.picks_count):
                if i < len(decks):
                    leader_label = get_card_localization(
                        cards=self.cards_data,
                        card_id=decks[i]['leaderId'],
                        faction_label=True
                    )

                    faction_leaders.add(leader_label)

        # header - later

        abilities_label = 'Abilities'
        coins_label = 'Coins'

        worksheet.merge_range(row_index, 0, row_index + 1, 0, abilities_label, default_format)
        worksheet.merge_range(row_index, 1, row_index + 1, 1, coins_label, default_format)

        maximum_games = 5  # change to fetching field `bestOfX`
        col_index = leader_col_start

        total_col_length = 0
        for api_bracket in api_brackets:
            for bracket in self.games[api_bracket]:
                total_col_length += len(bracket['games'])

        worksheet.set_column(0, 0, 20)
        worksheet.set_column(1, 1, 5)
        worksheet.set_column(leader_col_start, total_col_length * maximum_games + 1, 3)
        worksheet.set_column(total_col_length * maximum_games + 2, total_col_length * maximum_games + 2 + 6, 15)

        for api_bracket in api_brackets:
            label = '{} bracket # {}'

            for bracket in self.games[api_bracket]:
                merge_cols = len(bracket['games'])
                worksheet.merge_range(row_index, col_index, row_index, col_index + merge_cols * maximum_games - 1,
                                      label.format(
                                          api_bracket, bracket['round']
                                      ), default_format)

                col_index += merge_cols * maximum_games

        # other headers
        matches_label = 'Matches'
        matches_percentage_label = 'Matches %'
        coins_amount_label = 'Coins number'
        wins_on_coin_label = 'Wins on coins'
        wins_label = 'Wins'
        winrate_on_coin_label = 'Winrate on coin'
        winrate_label = 'Winrate'
        faction_label = 'Faction'
        faction_matches_label = 'Faction matches'
        faction_matches_percentage_label = 'Faction matches %'
        faction_wins_label = 'Faction wins'
        faction_wins_percentage_label = 'Faction wins %'

        worksheet.merge_range(row_index, col_index, row_index + 3, col_index, matches_label, default_format)
        worksheet.merge_range(row_index, col_index + 1, row_index + 3, col_index + 1, matches_percentage_label,
                              default_format)
        worksheet.merge_range(row_index, col_index + 2, row_index + 3, col_index + 2, coins_amount_label,
                              default_format)
        worksheet.merge_range(row_index, col_index + 3, row_index + 3, col_index + 3, wins_on_coin_label,
                              default_format)
        worksheet.merge_range(row_index, col_index + 4, row_index + 3, col_index + 4, wins_label, default_format)
        worksheet.merge_range(row_index, col_index + 5, row_index + 3, col_index + 5, winrate_on_coin_label,
                              default_format)
        worksheet.merge_range(row_index, col_index + 6, row_index + 3, col_index + 6, winrate_label, default_format)

        worksheet.merge_range(row_index, col_index + 7, row_index + 3, col_index + 7, faction_label, default_format)
        worksheet.merge_range(row_index, col_index + 8, row_index + 3, col_index + 8, faction_matches_label,
                              default_format)
        worksheet.merge_range(row_index, col_index + 9, row_index + 3, col_index + 9, faction_matches_percentage_label,
                              default_format)
        worksheet.merge_range(row_index, col_index + 10, row_index + 3, col_index + 10, faction_wins_label,
                              default_format)
        worksheet.merge_range(row_index, col_index + 11, row_index + 3, col_index + 11, faction_wins_percentage_label,
                              default_format)

        row_index += 1
        col_index = leader_col_start

        for api_bracket in api_brackets:
            for index, game_bracket in enumerate(self.games[api_bracket]):
                games = game_bracket['games']
                for game_index, entry in enumerate(games):
                    first_player = entry['players'][0]
                    second_player = entry['players'][1]

                    worksheet.merge_range(row_index, col_index, row_index,
                                          col_index + maximum_games - 2, first_player['name'])
                    worksheet.write_number(row_index, col_index + maximum_games - 1, entry["score"][0])

                    worksheet.merge_range(row_index + 1, col_index, row_index + 1,
                                          col_index + maximum_games - 2, second_player['name'])
                    worksheet.write_number(row_index + 1, col_index + maximum_games - 1,
                                           entry["score"][1])

                    for round_index in range(len(entry['games'])):
                        worksheet.write_number(row_index + 2, col_index + round_index, round_index + 1)

                    col_index += maximum_games

        row_index += 3  # temporally

        blue_coin_format = self.workbook.add_format({
            'pattern': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': coin_color[0]
        })

        red_coin_format = self.workbook.add_format({
            'pattern': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': coin_color[1]
        })

        blue_coin_percentage_format = self.workbook.add_format({
            'pattern': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': coin_color[0],
            'num_format': 10
        })

        red_coin_percentage_format = self.workbook.add_format({
            'pattern': 1,
            'align': 'center',
            'valign': 'vcenter',
            'bg_color': coin_color[1],
            'num_format': 10
        })

        cell_content_format = self.workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
        })

        content_percentage_format = self.workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'num_format': 10
        })

        leaders_references = {}
        used_leaders = {faction: [] for faction in Factions}

        # copy link to the start of leader parsing
        leader_begging_row = row_index

        # faction leaders
        for faction in self.factions_order['order']:
            faction_color = faction_colors[faction]

            for card_id in self.factions_order['factions'][faction]:
                leader_label = get_card_localization(
                    cards=self.cards_data,
                    card_id=str(card_id),
                    faction_label=True
                )

                if leader_label in faction_leaders:
                    content_format = self.workbook.add_format({
                        'pattern': 1,
                        'align': 'left',
                        'valign': 'vcenter',
                        'bg_color': faction_color
                    })

                    worksheet.merge_range(row_index, 0, row_index + 1, 0, '', cell_format=content_format)
                    worksheet.write_string(row_index, 0, leader_label, cell_format=content_format)

                    leaders_references[int(card_id)] = row_index
                    used_leaders[faction].append(int(card_id))

                    # red blue coin
                    worksheet.write_string(row_index, 1, 'Blue', blue_coin_format)
                    worksheet.write_string(row_index + 1, 1, 'Red', red_coin_format)

                    # apply red/blue till the end
                    for i in range(2, total_col_length * maximum_games + 2):
                        worksheet.write(row_index, i, None, blue_coin_format)
                        worksheet.write(row_index + 1, i, None, red_coin_format)

                    row_index += 2

        col_index = leader_col_start

        # parse games result to factions
        for api_bracket in api_brackets:
            for index, game_bracket in enumerate(self.games[api_bracket]):
                games = game_bracket['games']
                for game_index, entry in enumerate(games):
                    first_player = entry['players'][0]
                    second_player = entry['players'][1]

                    coins = []
                    first = 0
                    second = 1

                    if 'playerGoingFirst' in entry:
                        if second_player['id'] == entry['playerGoingFirst']:
                            first, second = second, first

                        for i in range(len(entry['games'])):
                            coins.append((first, second))
                            first, second = second, first

                        for round_index, game in enumerate(entry['games']):
                            if game['result']:
                                current_app.logger.debug(game)
                                current_app.logger.debug(f'{first_player["name"]} | {second_player["name"]}')

                                result1, result2 = 1, 0
                                if game['result'] == -1:
                                    result1, result2 = result2, result1

                                leader_first = _get_game_bracket_leader(game, "1")
                                leader_second = _get_game_bracket_leader(game, "2")

                                if leader_first:
                                    color = blue_coin_format if coins[round_index][0] == 0 else red_coin_format
                                    worksheet.write_number(leaders_references[leader_first] + coins[round_index][0],
                                                           col_index + round_index, result1, color)

                                if leader_second:
                                    color = blue_coin_format if coins[round_index][1] == 0 else red_coin_format
                                    worksheet.write_number(leaders_references[leader_second] + coins[round_index][1],
                                                           col_index + round_index, result2, color)

                    col_index += maximum_games

        # calculate leader abilities
        row_index = leader_begging_row

        leader_count_col = total_col_length * maximum_games + 2
        total_games_cell = xl_rowcol_to_cell(row_index + len(leaders_references) * 2, leader_count_col, True, True)

        for leader_row in leaders_references.values():
            # leader total usage
            leader_range = xl_range(leader_row, leader_col_start, leader_row + 1, total_col_length * maximum_games + 1)
            worksheet.merge_range(leader_row, leader_count_col, leader_row + 1, leader_count_col,
                                  f'=COUNTA({leader_range})', cell_content_format)

            # leader total usage in percentage
            current_leader_count_cell = xl_rowcol_to_cell(leader_row, leader_count_col)
            worksheet.merge_range(
                leader_row,
                leader_count_col + 1,
                leader_row + 1,
                leader_count_col + 1,
                f"={current_leader_count_cell}/{total_games_cell}",
                content_percentage_format
            )

            # coins total amount
            blue_range = xl_range(leader_row, leader_col_start, leader_row, total_col_length * maximum_games + 1)
            red_range = xl_range(leader_row + 1, leader_col_start, leader_row + 1, total_col_length * maximum_games + 1)
            worksheet.write_formula(leader_row, leader_count_col + 2, f"=COUNTA({blue_range})", blue_coin_format)
            worksheet.write_formula(leader_row + 1, leader_count_col + 2, f"=COUNTA({red_range})", red_coin_format)

            # coins win amount
            worksheet.write_formula(leader_row, leader_count_col + 3, f"=COUNTIF({blue_range},1)", blue_coin_format)
            worksheet.write_formula(leader_row + 1, leader_count_col + 3, f"=COUNTIF({red_range},1)", red_coin_format)

            # leader win amount
            coins_range = xl_range(leader_row, leader_count_col + 3, leader_row + 1, leader_count_col + 3)
            worksheet.merge_range(leader_row, leader_count_col + 4, leader_row + 1, leader_count_col + 4,
                                  f"=SUM({coins_range})", cell_content_format)

            # leader coin winrate
            blue_coin_win_cell = xl_rowcol_to_cell(leader_row, leader_count_col + 3)
            blue_count_total_cell = xl_rowcol_to_cell(leader_row, leader_count_col + 2)

            red_coin_win_cell = xl_rowcol_to_cell(leader_row + 1, leader_count_col + 3)
            red_count_total_cell = xl_rowcol_to_cell(leader_row + 1, leader_count_col + 2)

            worksheet.write_formula(leader_row, leader_count_col + 5, f"={blue_coin_win_cell}/{blue_count_total_cell}",
                                    blue_coin_percentage_format)

            worksheet.write_formula(leader_row + 1, leader_count_col + 5,
                                    f"={red_coin_win_cell}/{red_count_total_cell}", red_coin_percentage_format)

            # leader total winrate
            total_leader_amount_cell = xl_rowcol_to_cell(leader_row, leader_count_col)
            wins_leader_amount_cell = xl_rowcol_to_cell(leader_row, leader_count_col + 4)

            worksheet.merge_range(leader_row, leader_count_col + 6, leader_row + 1, leader_count_col + 6,
                                  f"={wins_leader_amount_cell}/{total_leader_amount_cell}", content_percentage_format)

        total_games_formula = f"""=SUM({xl_range(leader_begging_row, leader_count_col,
                                                 leader_begging_row + len(leaders_references) * 2 - 1,
                                                 leader_count_col)})"""

        total_wins_formula = f"""=SUM({xl_range(leader_begging_row, leader_count_col + 4,
                                                leader_begging_row + len(leaders_references) * 2 - 1,
                                                leader_count_col + 4)})"""

        bottom_row = leader_begging_row + len(leaders_references) * 2

        worksheet.write_formula(bottom_row, leader_count_col, total_games_formula,
                                cell_content_format)

        total_wins_cell = xl_rowcol_to_cell(bottom_row, leader_count_col + 4)
        worksheet.write_formula(bottom_row, leader_count_col + 4,
                                total_wins_formula,
                                cell_content_format)

        # coins data
        coins = []
        for i in range(leader_begging_row, leader_begging_row + len(leaders_references) * 2, 2):
            coins.append((
                xl_rowcol_to_cell(i, leader_count_col + 3),
                xl_rowcol_to_cell(i + 1, leader_count_col + 3)
            ))

        blue_coin_cells = ",".join(list(map(lambda x: x[0], coins)))
        red_coin_cells = ",".join(list(map(lambda x: x[1], coins)))

        # coin data
        worksheet.write_string(bottom_row + 1, leader_count_col + 2, 'Blue wins', blue_coin_format)
        worksheet.write_string(bottom_row + 2, leader_count_col + 2, 'Red wins', red_coin_format)

        red_coin_wins_cell = xl_rowcol_to_cell(bottom_row + 1, leader_count_col + 3)
        blue_coin_wins_cell = xl_rowcol_to_cell(bottom_row + 2, leader_count_col + 3)
        worksheet.write_formula(bottom_row + 1, leader_count_col + 3, f'=SUM({blue_coin_cells})', cell_content_format)
        worksheet.write_formula(bottom_row + 2, leader_count_col + 3, f'=SUM({red_coin_cells})', cell_content_format)

        worksheet.write_string(bottom_row + 1, leader_count_col + 4, 'Blue wins %', blue_coin_format)
        worksheet.write_string(bottom_row + 2, leader_count_col + 4, 'Red wins %', red_coin_format)

        worksheet.write_formula(bottom_row + 1, leader_count_col + 5, f'={red_coin_wins_cell}/{total_wins_cell}',
                                blue_coin_percentage_format)
        worksheet.write_formula(bottom_row + 2, leader_count_col + 5, f'={blue_coin_wins_cell}/{total_wins_cell}',
                                red_coin_percentage_format)

        row_index = leader_begging_row

        # factions parsing
        for faction in self.factions_order['order']:
            # get first and last rows
            first_leader, last_leader = used_leaders[faction][0], used_leaders[faction][-1]
            first_row, last_row = leaders_references[first_leader], leaders_references[last_leader]

            worksheet.merge_range(first_row, leader_count_col + 7, last_row + 1, leader_count_col + 7, faction.value,
                                  cell_content_format)

            faction_games_cell_range = xl_range(first_row, leader_count_col, last_row + 1, leader_count_col)
            faction_games_cell = xl_rowcol_to_cell(first_row, leader_count_col + 8, last_row + 1, leader_count_col + 8)
            worksheet.merge_range(first_row, leader_count_col + 8, last_row + 1, leader_count_col + 8,
                                  f'=SUM({faction_games_cell_range})', cell_content_format)
            worksheet.merge_range(first_row, leader_count_col + 9, last_row + 1, leader_count_col + 9,
                                  f'={faction_games_cell}/{total_games_cell}', content_percentage_format)

            faction_wins_cell_range = xl_range(first_row, leader_count_col + 4, last_row + 1, leader_count_col + 4)
            faction_wins_cell = xl_rowcol_to_cell(first_row, leader_count_col + 10, last_row + 1, leader_count_col + 10)
            worksheet.merge_range(first_row, leader_count_col + 10, last_row + 1, leader_count_col + 10,
                                  f'=SUM({faction_wins_cell_range})',
                                  cell_content_format)
            worksheet.merge_range(first_row, leader_count_col + 11, last_row + 1, leader_count_col + 11,
                                  f'={faction_wins_cell}/{total_wins_cell}', content_percentage_format)
